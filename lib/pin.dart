import 'package:flutter/material.dart';

class PinBar extends StatefulWidget {
  @override
  _PinBarState createState() => _PinBarState();
}

class _PinBarState extends State<PinBar> {
  String pintemp = "";
  void pinverif(String pin) {
    if (pin.length < 6) {
      setState(() {
        pintemp += pin;
      });
    }
    if (pintemp == "123456"){
      print("Pin Verified");
    }
  }

  void deletepin() {
    if (pintemp.length > 0) {
      setState(() {
        pintemp = pintemp.substring(0, pintemp.length - 1);
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment(0, 0.1),
              end: Alignment.bottomCenter,
              colors: <Color>[
            Color(0xFF40c4ff),
            Color(0xFFBEEBFF),
          ])),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text(
            "Pin Verification",
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.w800, color: Colors.white),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: pintemp.length < 1 ? Colors.transparent : Colors.white,
                  border: Border.all(color: Colors.white, width: 3),
                  borderRadius: BorderRadius.all(new Radius.circular(5)),
                ),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: pintemp.length < 2 ? Colors.transparent : Colors.white,
                  border: Border.all(color: Colors.white, width: 3),
                  borderRadius: BorderRadius.all(new Radius.circular(5)),
                ),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: pintemp.length < 3 ? Colors.transparent : Colors.white,
                  border: Border.all(color: Colors.white, width: 3),
                  borderRadius: BorderRadius.all(new Radius.circular(5)),
                ),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: pintemp.length < 4 ? Colors.transparent : Colors.white,
                  border: Border.all(color: Colors.white, width: 3),
                  borderRadius: BorderRadius.all(new Radius.circular(5)),
                ),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: pintemp.length < 5 ? Colors.transparent : Colors.white,
                  border: Border.all(color: Colors.white, width: 3),
                  borderRadius: BorderRadius.all(new Radius.circular(5)),
                ),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: pintemp.length < 6 ? Colors.transparent : Colors.white,
                  border: Border.all(color: Colors.white, width: 3),
                  borderRadius: BorderRadius.all(new Radius.circular(5)),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                child: Text(
                  "1",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("1"),
              ),
              FlatButton(
                child: Text(
                  "2",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("2"),
              ),
              FlatButton(
                child: Text(
                  "3",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("3"),
              ),
            ],
          ),
           Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                child: Text(
                  "4",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("4"),
              ),
              FlatButton(
                child: Text(
                  "5",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("5"),
              ),
              FlatButton(
                child: Text(
                  "6",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("6"),
              ),
            ],
          ),
           Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                child: Text(
                  "7",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("7"),
              ),
              FlatButton(
                child: Text(
                  "8",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("8"),
              ),
              FlatButton(
                child: Text(
                  "9",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("9"),
              ),
            ],
          ),
           Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                child: null,
                onPressed: () => null,
              ),
              FlatButton(
                child: Text(
                  "0",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                ),
                onPressed: () => pinverif("0"),
              ),
              FlatButton(
                child: Icon(Icons.backspace, color: Colors.white, size: 40,),
                onPressed: () => deletepin(),
              ),
            ],
          )
        ],
      ),
    );
  }
}