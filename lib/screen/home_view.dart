import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:kigspay/screen/test_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Image.asset(
                'asset/image/man.png',
                fit: BoxFit.cover,
                width: 50,
                height: 50,
              ),
            ],
          ),
          backgroundColor: Colors.blueAccent,
          leading: IconButton(
            icon: Icon(Icons.dehaze),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Column(
                  children: <Widget>[
                    Image.asset(
                      'asset/image/man.png',
                      width: 70,
                      height: 70,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text('Hufflepof'),
                    Text('Hufflepof@lalala.com')
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
              ),
              ListTile(
                title: Text('Account'),
              ),
              ListTile(
                title: Text('Help'),
              ),
              ListTile(
                title: Text('User Feedback'),
              ),
              SizedBox(
                height: 80.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Material(
                  borderRadius: BorderRadius.circular(30.0),
                  shadowColor: Colors.blueAccent,
                  elevation: 5.0,
                  color: Colors.blueAccent,
                  child: MaterialButton(
                    minWidth: 200.0,
                    height: 42.0,
                    onPressed: null,
                    child: Text(
                      'LOG OUT',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        body: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(3.0)),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(3.0),
                            topRight: Radius.circular(3.0))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'SALDO',
                          style: TextStyle(
                              fontSize: 15.0, fontWeight: FontWeight.bold),
                        ),
                        Container(
                          child: Text(
                            '12.000',
                            style: TextStyle(
                                fontSize: 15.0, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),

                  //this is carousel

                  Column(
                    children: <Widget>[
                      Container(
                        child: CarouselSlider(
                          height: 200.0,
                          autoPlay: true,
                          initialPage: 0,
                          items: [
                            "asset/image/slider_1.jpg",
                            "asset/image/slider_2.jpg",
                            "asset/image/slider_3.jpg",
                          ].map((i) {
                            return Builder(
                              builder: (BuildContext context) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: EdgeInsets.symmetric(vertical: 5.0),
                                  decoration: BoxDecoration(
                                      color: Colors.blueAccent,
                                      image: DecorationImage(
                                          image: AssetImage(i))),
                                );
                              },
                            );
                          }).toList(),
                        ),
                      )
                    ],
                  ),

                  //GridView
                  SizedBox(
                    height: 200,
                    child: GridView.count(
                      crossAxisCount: 3,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(8.0),
                          alignment: Alignment.center,
                          child: InkWell(
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  'asset/image/top-up.png',
                                  width: 50,
                                  height: 50,
                                ),
                                Padding(padding: EdgeInsets.only(top: 5.0)),
                                Text('TOP UP',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold))
                              ],
                            ),
                            onTap: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (_) {
                                return Test();
                              }));
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(8.0),
                          alignment: Alignment.center,
                          child: InkWell(
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  'asset/image/qr-code.png',
                                  width: 50,
                                  height: 50,
                                ),
                                Padding(padding: EdgeInsets.only(top: 5.0)),
                                Text('SCAN',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold))
                              ],
                            ),
                            onTap: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (_) {
                                return Test();
                              }));
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(8.0),
                          alignment: Alignment.center,
                          child: InkWell(
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                  'asset/image/menu.png',
                                  width: 50,
                                  height: 50,
                                ),
                                Padding(padding: EdgeInsets.only(top: 5.0)),
                                Text(
                                  'ORDER',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            onTap: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (_) {
                                return Test();
                              }));
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
